import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'doc',
      component: () => import('@/views/doc/doc')
      // component: require('@/components/LandingPage').default
    },
    // {
    //   path: '/',
    //   name: 'download',
    //   component: () => import('@/views/transfer/download')
    //   // component: require('@/components/LandingPage').default
    // },
    // {
    //   path: '/',
    //   name: 'upload',
    //   component: () => import('@/views/transfer/upload')
    //   // component: require('@/components/LandingPage').default
    // },
    // {
    //   path: '/',
    //   name: 'landing-page',
    //   component: () => import('@/components/LandingPage')
    //   // component: require('@/components/LandingPage').default
    // },
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/notify',
      name: 'notify',
      component: () => import('@/views/notify/index')
      // component: require('@/views/notify/index').default
    },
    {
      path: '/suspension',
      name: 'suspension',
      component: () => import('@/views/components/suspension')
      // component: require('@/views/components/suspension').default
    },
    {
      path: '/completeTransfer',
      name: 'completeTransfer',
      component: () => import('@/views/transfer/complete')
      // component: require('@/views/components/suspension').default
    }
  ]
})
